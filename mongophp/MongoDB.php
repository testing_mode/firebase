<?php
require_once('const.php');

class MongoDB {
	
	private $_conn;
	private $_dbname;
	
	public function __construct(){
		// Connect to MongoDB 
		try {

			$this->_conn = new MongoDB\Driver\Manager("mongodb://".MONGO_HOST.":".MONGO_PORT);
			$this->_dbname = MONGO_DB;

		} catch (MongoDB\Driver\Exception\Exception $e) {
			
			die('Error:' . $e->getMessage());
		}
	}
	
	public function mongoInsert($collectionName, $data){
		$inserts = new MongoDB\Driver\BulkWrite();
		$inserts->insert($data);

		$this->_conn->executeBulkWrite($this->_dbname.".$collectionName", $inserts);
	}
	
	public function mongoFetch($collectionName){
		$filter = [];
		$option = [];
		$read = new MongoDB\Driver\Query($filter, $option);

		$all_users = $this->_conn->executeQuery($this->_dbname.".$collectionName", $read);
		
	}
	
	public function mongoUpdate($collectionName){
		$data = array(
						'last_name' => 'vikas',
						'$set' => array('first_name' => 'Liton', 'last_name' => 'Sarkar'),
						'multi' => false, 
						'upsert' => true
					);
		$single_update = new MongoDB\Driver\BulkWrite();
		$single_update->update( ['last_name' => 'Roy'],
								['$set' => ['first_name' => 'Liton', 'last_name' => 'Sarkar']],
								['multi' => false, 'upsert' => false]
							  );
		$result = $this->_conn->executeBulkWrite($this->_dbname.".$collectionName", $single_update);

		if($result) {
			echo nl2br("Record updated successfully\n");
		}
	}
	
	public function mongoDelete($collectionName){
		$delete = new MongoDB\Driver\BulkWrite();
		$delete->delete(
							['first_name' => 'Liton'],
							['limit' => 0]
						);
		
		$result = $this->_conn->executeBulkWrite($this->_dbname.".$collectionName", $delete);				
		if($result){
			echo nl2br("Record deleted successfully\n");
		}
	}


}
