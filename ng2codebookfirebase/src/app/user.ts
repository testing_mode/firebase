export class User {
  name: string ;
  username: string ;
  email: string ;
  address: string ;
  phone: string ;
  website: string ;
  company: string ;
}
