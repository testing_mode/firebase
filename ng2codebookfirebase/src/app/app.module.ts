import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { UserComponent } from './user.component';
import { routing } from './app.routing';


export const firebaseConfig = {
  apiKey: "AIzaSyDE-SXExsL1BXYl4kFmpC5RWtr7jm8gzMA",
  authDomain: "ngcrudproject.firebaseapp.com",
  databaseURL: "https://ngcrudproject.firebaseio.com",
  projectId: "ngcrudproject",
  storageBucket: "",
  messagingSenderId: "119364581454",
  appId: "1:119364581454:web:771208de58d420e8"
};

@NgModule({
  declarations: [
    AppComponent,
    UserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
