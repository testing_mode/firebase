import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireList } from '@angular/fire/database';

import  { User } from './user';

@Component({
  selector: 'user-form',
  templateUrl:'user-form.component.html'
})


export class UserFormComponent implements OnInit{
  form: FormGroup;
  title:string;
  user = new User();


  constructor(private fb: FormBuilder, private _router: Router, private af:AngularFireDatabase){
    this.form = fb.group({
      username:['',Validators.required],
      email:['',Validators.required],
    })
  }

  ngOnInit(){
    this.title = " New User";
  }


}
