import { Component, OnInit } from '@angular/core';
import { UserService } from './user.service';
import { map } from 'rxjs/operators';

import { Router } from '@angular/router';

@Component ({
  selector: 'users',
  templateUrl:'./user.component.html',
})

export class UserComponent implements OnInit {
  users: any;

  constructor(private userService: UserService, private _router: Router){

  }

  ngOnInit(){
    this.getUserList();
  }

  getUserList() {
    this.userService.getUserList().snapshotChanges().pipe(
      map(changes =>
        changes.map(c =>
          ({ key: c.payload.key, ...c.payload.val() })
        )
      )
    ).subscribe(customers => {
      this.users = customers;
    });
  }

  add(){
    this._router.navigate(['add']);
  }

  delete(user){
    if(confirm("Are you sure you want to delete " + user.name + " ?")){
      
    }
  }
}
